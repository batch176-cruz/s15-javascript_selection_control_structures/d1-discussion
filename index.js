console.log("Hello World!");
// alert("Hello, Batch 176")

// Assignment Operator
// Basic Assignment Operator (=)
let assignmentNumber = 8;
console.log(assignmentNumber);

// Addition Assignment Operator (+=)
console.log("Add 2");
assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber);

console.log("Add 2");
assignmentNumber += 2;
console.log(assignmentNumber);

console.log("Increase 1");
assignmentNumber++;
console.log(assignmentNumber);

//Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)
console.log("Subtract 2");
assignmentNumber -= 2;
console.log(assignmentNumber);

console.log("Multiply by 2");
assignmentNumber *= 2;
console.log(assignmentNumber);

console.log("Divided by 2");
assignmentNumber /= 2;
console.log(assignmentNumber);

console.log("Subtract 3");
assignmentNumber -= 3;
console.log(assignmentNumber);

// Arithmetic Operators
let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;
console.log(sum);

// Subtraction Operator
let difference = y - x;
console.log(difference);

// Multiplication Operator
let product = x * y;
console.log(product);

// Division Operaror
let quotient = y / x;
console.log(quotient);

// Modulo Operator
let remainder = y % x;
console.log(remainder);

// Multiple Operators
// PEMDAS (Parenthesis, Exponent, Multiplication and Division, Addition and Subtraction)

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

let pemdas2 = 2 + 4 - 2 ** (10 / 5);
console.log(pemdas2);	// 2

// Increment and Decrement Operator
let z = 1;

// Pre-increment
let increment = ++z;
console.log(increment);	// 2
console.log(z);			// 2

// Post-increment
increment = z++;
console.log(increment);	// 2
console.log(z);			// 3

// Pre-decrement
let decrement = --z;
console.log(decrement);	// 2
console.log(z);			// 2

// Post-decrement
decrement = z--;
console.log(decrement);	// 2
console.log(z);			// 1

// What happens if we add a string and a number?
let numA = '10';
let numB = 12;

// Type Coercion
let coercion = numA + numB;
console.log(coercion);			// 1012
console.log(typeof(numA));		// string
console.log(typeof(numB));		// number
console.log(typeof(coercion));	// string

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);		// 30

// Adding Boolean and Number
let numE = true + 1;
console.log(numE);			// 2
console.log(typeof(numE));	// number

let numZ = null + undefined;
console.log(numZ);			// NaN (Not a Number)

let numY = true * 5;
console.log(numY);			// 5

let numF = false + 1;
console.log(numF);			// 1

// Comparison Operator
// Equality Operator (==) - checks whether the operands are equal or have the same content
// Returns a boolean value
let juan = 'juan';
console.log(1 == 1);					// true
console.log(1 == "1");					// true
console.log(1 == 2);					// false
console.log(0 == false);				// true
console.log("juan" == "juan");			// true
console.log("juan" == juan);			// true

// Inequality Operator (!=) - checks whether the operands are not equal/have different content
console.log(1 != 1);					// false
console.log(1 != 2);					// true
console.log(1 != "1");					// false
console.log(0 != false);				// false
console.log("juan" != "juan");			// false
console.log("juan" != juan);			// false

// Strict Equality Operator (===) - checks whether the operands are equal or have the same content and data type
console.log(1 === 1);					// true
console.log(1 === "1");					// false
console.log(1 === 2);					// false
console.log(0 === false);				// false
console.log("juan" === "juan");			// true
console.log("Juan" === "juan");			// false
console.log("juan" === juan);			// true

// Strict Inequality Operator (!==) - checks whether the operands are not equal/have the same content and compares the data type
console.log(1 !== 1);					// false
console.log(1 !== 2);					// true
console.log(1 !== "1");					// true
console.log(0 !== false);				// true
console.log(null !== undefined);		// true
console.log('juan' !== 'juan');			// false
console.log('juan' !== juan);			// false

// Logical Operator
let isLegalAge = true;
let isRegistered = false;

// AND Operator (&&)
// Returns true if all operands are true
let allRequirements = isLegalAge && isRegistered;
console.log(allRequirements);			// false

// OR Operator (||)
// Returns true if one of the operands is true
let someRequirements = isLegalAge || isRegistered;
console.log(someRequirements);			// true

// NOT Operator (!)
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet);	// true

// Mini Activity:

	// Without using console, try to analyze the logic manually

	let isTrue = true;
	let isFalse = false;

	let falses = false && false;
	console.log(falses);

	let isTralse = !(isTrue || isFalse && isFalse);
				// !(TRUE || FALSE)
				// !(TRUE)	
	console.log(isTralse);				// false


// Control Structures - sorts out whether the statement/s are to be executed base on the condition, whether its true or false
/*
	if else statement
	switch statement
*/

// if .. else statement
/*
	Syntax:
		if (condition)
		{
			statement;
		}
		else
		{
			statement;
		}
*/

// if statement
// executes a statement if a specified condition is true
// can stand alone even without the else statement
/*
	Syntax:
		if (condition)
		{
			code block;
		}
*/

// <	-	less than
// >	-	greater than
// <=	-	less than or equal
// >=	-	greater than or equal

let numG = -1;

if (numG < 0)
{
	console.log("Yow my dudes!");
}

if (false == "1")
{
	console.log("This statement will not be printed.")
}

// else if clause
// executes a statement if previous conditions are false and if the specified condition is true
// The "else if clause" is optional and can be added to capture additional conditions

let numH = 1;

if (numG > 0)
{
	console.log("Hello!");
}
else if (numH > 0)
{
	console.log("World!");
}

// else statement
// executes a statement if all other conditions are false
// the "else" statement is optional and can be added to capture any other result to change the flow of a program
// cannot stand alone

if (numG > 0)
{
	console.log("Hello Mah Dudes!");
}
else if (numH = 0)
{
	console.log("World News");
}
else
{
	console.log("Again!");
}

// let age = prompt("Enter your age:");
let age = 20;

if (age <= 18)
{
	console.log("Not allowed to drink");
}
else
{
	console.log("Shot!")
}


/*
	Mini Activity:
	Create a conditional statement that if height is below 150, display "Did not passed the minimum height requirement." If equal or above 150, display "Passed the minimum height requirement."

	Stretch Goal:
		Put it inside a function.
*/

let height = 160;

function heightTest(x)
{
	if (x < 150)
	{
		console.log("Did not passed the minimum height requirement.");
	}
	else
	{
		console.log("Passed the minimum height requirement.");
	}
}

heightTest(height);

// Solution 1
/*
	let height = 160;

	if (height < 150)
	{
		console.log("Did not passed the minimum height requirement.");
	}
	else
	{
		console.log("Passed the minimum height requirement.");
	}
*/

// Solution 2
/*
	function heightReq(h)
	{
		if (h < 150)
		{
			console.log("Did not passed");
		}
		else
		{
			console.log("Passed the req.");
		}
	}

	heightReq(140);
	heightReq(155);
*/

//if, else if, and else statement with functions

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed)
{
	if (windSpeed < 30)
	{
		return 'Not a typhoon yet.';
	}
	else if (windSpeed <= 61)
	{
		return 'Tropical depression detected.';
	}
	else if (windSpeed >= 62 && windSpeed <= 88)
	{
		return 'Tropical Storm Detected';
	}
	else if (windSpeed >= 80)
	{
		return 'Severe tropical storm detected.';
	}
	else
	{
		return 'Typhoon detected.';
	}
}

message = determineTyphoonIntensity(70);
console.log(message);

if (message == "Tropical Storm Detected")
{
	console.warn(message);
}

message = determineTyphoonIntensity(30);
console.log(message);


// Truthy and Falsy
/*
	In JS a "truthy" value is a value that is considered true when encountered in a Boolean context
	- Values are considered true unless defined otherwise.
	Falsy Values
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/

// Truthy
if (true)
{
	console.log("Truthy");
}

if (1)
{
	console.log("Truthy");
}

if ([])
{
	console.log("Truthy");
}

// Falsy
if (false)
{
	console.log("Falsy");
}
if (0)
{
	console.log("Falsy");
}
if (undefined)
{
	console.log("Falsy");
}


// Conditional (Ternary) Operator
/*
	It takes in 3 operands
	1. condition
	2. expression to execute if the condition is true
	3. expression to execute if the condition is false

	Syntax:
		(expression) ? ifTrue : ifFalse

*/

let ternaryResult = (1 > 18) ? "True1"
					: (3 < 5) ? "True2"
					: "False1"


// let ternaryResult = if (1 < 18) ? {true} : else {false};
console.log(ternaryResult)

let myAge = 20;
myAge >= 18 ? console.log("IF") : console.log("ELSE");


// Multiple Statement

let name;

function isOfLegalAge() {
	name = "John";
	return 'You are of the legal age limit'
}

function isUnderAge() {
	name = "Jane";
	return 'You are under the age limit'
}

let age1 = parseInt(prompt("What is your age?"));
console.log(age1);
let legalAge = (age1 > 18) ? isOfLegalAge() : isUnderAge()
console.log(legalAge + ", " + name);


// Switch Statement
/*
	Syntax:
		switch (expression) {
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}

*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case 'monday', 'lunes':
		console.log("The color of the day is red.");
		break;
	case 'tuesday':
		console.log("The color of the day is orange.");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow.");
		break;
	case 'thursday':
		console.log("The color of the day is green.");
		break;
	case 'friday':
		console.log("The color of the day is blue.");
		break;
	case 'saturday':
		console.log("The color of the day is indigo.");
		break;
	case 'sunday':
		console.log("The color of the day is violet.");
		break;
	default:
		console.log("Please input a valid day.");
		break;
}

// Try Catch Finally Statement

	function showIntensityAlert(windSpeed) {
		try {
			alerat(determineTyphoonIntensity(windSpeed));
		}

		catch (error) {
			console.warn(error.message);
		}

		finally {
			alert("Intensity updates will show new alert.");
		}
	}

	showIntensityAlert(56);